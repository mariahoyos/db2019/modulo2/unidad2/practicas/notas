<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;


$hoy=date("d-m-Y");


?>

<div><p style="text-align:center; margin:20px 0 20px 0;color:#737373;"><?=$hoy?></p></div>

<div class="row">
<?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_listar',
            'layout'=>"{items}\n{summary}\n{pager}",
        ]);
?>
</div>