<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Notas;
use yii\jui\Accordion;
use yii\helpers\ArrayHelper;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionNotasfecha(){
        
        $todaslasfechas = Notas::getFechas();
        
        //var_dump(Notas::getNotasDeFecha($todaslasfechas->all()[0]->fecha));
        
//        $dataProvider = new ActiveDataProvider([
//            'query'=> Notas::getNotasDeFecha($todaslasfechas->all()[0]->fecha),
//        ]);
        
        $dataProvider = new ActiveDataProvider([
            'query'=> $todaslasfechas,
        ]);
        
        return $this->render('listar', [
            'dataProvider' => $dataProvider,
        ]);
        
    }
    
    public function actionNotasfecha1(){
        
        $todaslasfechas = Notas::getFechas()->all();
        foreach($todaslasfechas as $indice=>$registro){
            $items[$indice]["header"]=$registro->fecha;
            foreach ($todaslasfechas as $indice=>$registro){
                $items[$indice]["content"]='<p class="noticiasporfecha">'.implode('</p><p class="noticiasporfecha">',ArrayHelper::getColumn(Notas::getNotasdeFecha($registro->fecha)->all(), 'mensaje')).'</p>';
            }
        }
        
                
        return $this->render("listar_1",[
            "items"=>$items,
        ]);
        
        
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
        
        
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
