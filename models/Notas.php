<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notas".
 *
 * @property int $id
 * @property string $fecha
 * @property string $hora
 * @property string $mensaje
 * @property int $idUsuario
 *
 * @property Usuarios $usuario
 */
class Notas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora'], 'safe'],
            [['mensaje'], 'string'],
            [['idUsuario'], 'integer'],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['idUsuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'mensaje' => 'Mensaje',
            'idUsuario' => 'Id Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'idUsuario']);
    }
    
    public static function getFechas(){
        return self::find()->select("fecha")->distinct();
    }
    
    public static function getNotasdeFecha($fecha){
        return self::find()
                ->select("mensaje")
                ->where("fecha = '$fecha'");
    }
    
}
